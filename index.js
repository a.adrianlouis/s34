const express = require('express')

const app = express()

const port = 3000

app.use(express.json())

app.use(express.urlencoded({extended: true}))

//========================== 1 

app.get('/home', (request, response) => {
	response.send('Welcome to the home page!')
})

//========================== 2 

let users = []; // Mock database

app.get('/home/users', (request, response) => {
	if(request.body.username !== '' && request.body.passowrd !== ''){
		users.push(request.body)

		response.send(`User ${request.body.username} successfully registered!`)
	} 
	else {
		response.send("Please input BOTH username and password.")
	}
})

//========================== 3


app.delete('/delete-user', (request, response) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			message = `User ${users[i].username} has been deleted!`
			
			users.splice(i, 1)
			break
		} 
		else {
			message = `User does not exist!`
		}
	}
	response.send(message)
})


app.listen(port, () => console.log(`Server is running at localhost: ${port}`))